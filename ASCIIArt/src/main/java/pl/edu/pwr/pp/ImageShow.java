package pl.edu.pwr.pp;

import javax.swing.JLabel;

public class ImageShow {
	
	
	public void showInMainActivity(char[][] ascii, JLabel img) {
	
		String text = "";
		String image = "";
		int rows = ascii.length;
		int columns = ascii[0].length;
		
		for(int i=0;i<rows;i++){
			text="";
			for(int j=0;j<columns;j++){
				text += ascii[i][j] ;
			}
			image+=text+"<br>";
		}
		image="<html>"+image+"</html>";
		img.setText(image);
		
	}
	

}
