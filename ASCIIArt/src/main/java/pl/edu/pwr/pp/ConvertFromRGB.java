package pl.edu.pwr.pp;

import java.awt.Color;
import java.awt.image.BufferedImage;

import javax.swing.Icon;
import javax.swing.JLabel;

public class ConvertFromRGB {
	
	
	public float[][] convert(JLabel img){
		Icon ico = img.getIcon();
		BufferedImage image = new BufferedImage(ico.getIconWidth(), ico.getIconHeight(), BufferedImage.TYPE_INT_RGB);
		
		float[][] intensities = null;
		int columns = ico.getIconWidth();
		int rows = ico.getIconHeight();
		
		intensities = new float[rows][];
		for (int i = 0; i < rows; i++) {
			intensities[i] = new float[columns];
		}
		
		float pxColor;
		for(int i=0;i<rows;i++){
			for(int j=0;j<columns;j++){
				Color color = new Color(image.getRGB(i, j));
				pxColor= (float) (0.2989 * color.getRed() + 0.5870 * color.getGreen() + 0.1140 * color.getBlue());
				intensities[rows][columns]=pxColor;
			}
		}
		
		return intensities;
	}

}
