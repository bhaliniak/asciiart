package pl.edu.pwr.pp;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;
import javax.swing.JPanel;

public class MainActivity {

	private JFrame frame;
	JLabel fileExtension;
	JLabel imaPGM;
	JButton saveBtn;
	JPanel imgPanel;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainActivity window = new MainActivity();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public MainActivity() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 652, 701);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JButton loadBtn = new JButton("Wczytaj obraz");
		loadBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				LoadActivity.NewScreen(imaPGM,saveBtn,frame,imgPanel,fileExtension);
				frame.setEnabled(false);
			}
		});
		loadBtn.setBounds(10, 242, 123, 23);
		frame.getContentPane().add(loadBtn);
		
		JButton btnOpcja = new JButton("[ Opcja 1 ]");
		btnOpcja.setEnabled(false);
		btnOpcja.setBounds(10, 276, 123, 23);
		frame.getContentPane().add(btnOpcja);
		
		JButton btnOpcja_1 = new JButton("[ Opcja 2 ]");
		btnOpcja_1.setEnabled(false);
		btnOpcja_1.setBounds(10, 310, 123, 23);
		frame.getContentPane().add(btnOpcja_1);
		
		saveBtn = new JButton("Zapisz do pliku");
		saveBtn.setEnabled(false);
		saveBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				JButton open = new JButton();
				JFileChooser fc = new JFileChooser();
				fc.setDialogTitle("Zapisz obrazek");
				fc.setCurrentDirectory(new java.io.File("C:/Users/"+System.getProperty("user.name")+"/Desktop"));
				if(fc.showSaveDialog(open)==JFileChooser.APPROVE_OPTION){
					JOptionPane.showMessageDialog(null, imaPGM.getText());
					if(fileExtension.getText().equals("pgm")){
						ImageSave imageSave = new ImageSave();
						imageSave.savePgmToTxt(fc.getSelectedFile().getAbsolutePath(), imaPGM.getText());
					}
					else{
						SaveAnotherImg saveAnotherImg = new SaveAnotherImg();
						saveAnotherImg.saveAnoherToTxt(fc.getSelectedFile().getAbsolutePath(), imaPGM);
					}
				}
			}
		});
		saveBtn.setBounds(10, 344, 123, 23);
		frame.getContentPane().add(saveBtn);
		
		JButton btnFunkcja = new JButton("[ Funkcja 1 ]");
		btnFunkcja.setEnabled(false);
		btnFunkcja.setBounds(10, 378, 123, 23);
		frame.getContentPane().add(btnFunkcja);
		
		JButton btnFunkcja_1 = new JButton("[ Funkcja 2 ]");
		btnFunkcja_1.setEnabled(false);
		btnFunkcja_1.setBounds(10, 412, 123, 23);
		frame.getContentPane().add(btnFunkcja_1);
		
		//imaPGM = new JTextArea();
		//imaPGM.setFont(new Font("Monospaced", Font.PLAIN, 5));
		//imaPGM.setBounds(511, 445, 115, 207);
		//frame.getContentPane().add(imaPGM);
		
		imaPGM = new JLabel("");
		imaPGM.setFont(new Font("Courier New", Font.PLAIN, 5));
		imaPGM.setBounds(158, 11, 468, 641);
		frame.getContentPane().add(imaPGM);
		
		imgPanel = new JPanel();
		imgPanel.setBounds(153, 25, 462, 493);
		frame.getContentPane().add(imgPanel);
		
		fileExtension = new JLabel("New label");
		fileExtension.setBounds(10, 36, 46, 14);
		fileExtension.setVisible(false);
		frame.getContentPane().add(fileExtension);
	}
}
