package pl.edu.pwr.pp;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import java.awt.Font;

import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.apache.maven.shared.utils.io.FileUtils;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFileChooser;

import java.awt.event.ActionListener;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.awt.event.ActionEvent;

public class LoadActivity {

	private JFrame frame;
	private static JFrame mFrame;
	static JLabel imaPGM;
	static JButton button;
	static JLabel imgExtension;
	private JTextField localAdress;
	private JTextField webAdress;
	private File file;
	JButton loadActionBtn;
	JButton loadFileBtn;
	static JPanel imgAnother;
	
	/**
	 * Launch the application.
	 */
	public static void NewScreen(JLabel mainImg,JButton mainBtn,JFrame mainFrame,JPanel mainAnotherImg,JLabel mainImgExtension) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					LoadActivity window = new LoadActivity();
					window.frame.setVisible(true);
					imgExtension=mainImgExtension;
					imaPGM = mainImg;
					button=mainBtn;
					mFrame=mainFrame;
					imgAnother=mainAnotherImg;
					window.frame.setAlwaysOnTop(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public LoadActivity() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 458, 266);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel labelLoadTitle = new JLabel("Wczytaj plik...");
		labelLoadTitle.setFont(new Font("Tahoma", Font.PLAIN, 13));
		labelLoadTitle.setBounds(10, 11, 90, 22);
		frame.getContentPane().add(labelLoadTitle);
		
		JRadioButton radioLocal = new JRadioButton("Z dysku");
		radioLocal.setBounds(32, 42, 109, 23);
		frame.getContentPane().add(radioLocal);
		
		JRadioButton radioWeb = new JRadioButton("Z adresu URL");
		radioWeb.setBounds(32, 99, 109, 23);
		frame.getContentPane().add(radioWeb);
		
		ButtonGroup buttonGroup = new ButtonGroup();
		buttonGroup.add(radioWeb);
		buttonGroup.add(radioLocal);
		
		radioLocal.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent event) {
		    	loadFileBtn.setEnabled(true);
		    	loadActionBtn.setEnabled(false);
		    	webAdress.setEditable(false);
		    }
		});
		
		radioWeb.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent event) {
		    	loadFileBtn.setEnabled(false);
		    	loadActionBtn.setEnabled(true);
		    	webAdress.setEditable(true);
		    }
		});
		
		localAdress = new JTextField();
		localAdress.setEditable(false);
		localAdress.setBounds(42, 72, 280, 20);
		frame.getContentPane().add(localAdress);
		localAdress.setColumns(10);
		
		webAdress = new JTextField();
		webAdress.setEditable(false);
		webAdress.setColumns(10);
		webAdress.setBounds(42, 129, 280, 20);
		frame.getContentPane().add(webAdress);
		
		loadFileBtn = new JButton("Wybierz plik...");
		loadFileBtn.setEnabled(false);
		loadFileBtn.setFont(new Font("Tahoma", Font.PLAIN, 10));
		loadFileBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.setVisible(false);
				JButton open = new JButton();
				JFileChooser fc = new JFileChooser();
				fc.setDialogTitle("Wybierz obrazek");
				fc.setCurrentDirectory(new java.io.File("C:/Users/"+System.getProperty("user.name")+"/Desktop"));
				//FileNameExtensionFilter filter = new FileNameExtensionFilter("PGM", "pgm");
				//fc.setFileFilter(filter);
				if(fc.showOpenDialog(open)==JFileChooser.APPROVE_OPTION){
					file = new File(fc.getSelectedFile().getAbsolutePath());
					localAdress.setText(file.getName());
					loadActionBtn.setEnabled(true);
				}
				else
					loadActionBtn.setEnabled(false);
				frame.setVisible(true);
				
			}
		});
		loadFileBtn.setBounds(332, 67, 101, 30);
		frame.getContentPane().add(loadFileBtn);
		
		loadActionBtn = new JButton("OK");
		loadActionBtn.setEnabled(false);
		loadActionBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				button.setEnabled(false);
				
				if(radioLocal.isSelected()){
					if(isExtensionPgm(file.getPath())){
						try {
							button.setEnabled(true);
							imaPGM = new JLabel("", null, JLabel.CENTER);
							imaPGM.setFont(new Font("Courier New", Font.PLAIN, 5));
							updateView(imaPGM);
							showPgmImage(file.getPath());
							mFrame.setEnabled(true);
							imgExtension.setText("pgm");
						} catch (URISyntaxException e) {
							e.printStackTrace();
						}
					}
					else{
						button.setEnabled(true);
						ImageIcon image = new ImageIcon(file.getPath());
						imaPGM = new JLabel("", image, JLabel.CENTER);
						updateView(imaPGM);
						mFrame.setEnabled(true);
						imgExtension.setText("jpg");
						//tutaj jpg itp
					}		
					frame.dispose();
				}
				
				else{//radioWeb is selected
					//http://agata.migalska.staff.iiar.pwr.wroc.pl/PP2016/obrazy/Schronisko_Sniezka.pgm
					if(isExtensionPgm(webAdress.getText())){
						button.setEnabled(true);
						File pathToTmpFile = new File("tmpfile.pgm");                
							try {
								FileUtils.copyURLToFile(new URL(webAdress.getText()), pathToTmpFile);
								imaPGM = new JLabel("", null, JLabel.CENTER);
								imaPGM.setFont(new Font("Courier New", Font.PLAIN, 5));
								updateView(imaPGM);
								showPgmImage("tmpfile.pgm");
								mFrame.setEnabled(true);
								imgExtension.setText("pgm");
							} catch (Exception e) {
								JOptionPane.showMessageDialog(null, "zabraklo szczescia z tym linkiem");
								//e.printStackTrace();
							}
						pathToTmpFile.delete();
					}
					else{
						button.setEnabled(true);
						File pathToTmpFile = new File("tmpfile.pgm"); 
						try {
							FileUtils.copyURLToFile(new URL(webAdress.getText()), pathToTmpFile);
							ImageIcon image = new ImageIcon("tmpfile.pgm");
							imaPGM = new JLabel("", image, JLabel.CENTER);
							updateView(imaPGM);
							mFrame.setEnabled(true);
							imgExtension.setText("jpg");
						} catch (IOException e) {
							JOptionPane.showMessageDialog(null, "zabraklo szczescia z tym linkiem");
						}
					}
					frame.dispose();
				}
			}
		});
		
		loadActionBtn.setBounds(107, 173, 101, 30);
		frame.getContentPane().add(loadActionBtn);
		
		JButton loadCancelBtn = new JButton("Anuluj");
		loadCancelBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mFrame.setEnabled(true);
				frame.dispose();
			}
		});
		loadCancelBtn.setBounds(248, 173, 101, 30);
		frame.getContentPane().add(loadCancelBtn);
			
	}
	
	
	public Boolean isExtensionPgm(String name){
		String extension = "";
		int i = name.lastIndexOf('.');
		if (i > 0) {
		    extension = name.substring(i+1);
		}
		if(extension.equals("pgm"))
			return true;
		else
			return false;	
	}
	
	public void showPgmImage(String fileName) throws URISyntaxException{
			ImageFileReader imageFileReader = new ImageFileReader(); 
			ImageShow imageShow = new ImageShow();
			int[][] intensities;
			intensities = imageFileReader.readPgmFile(fileName);
			char[][] ascii = ImageConverter.intensitiesToAscii(intensities);
			imageShow.showInMainActivity(ascii, imaPGM);
			button.setEnabled(true);
	}
	
	public void updateView(JLabel label){
		imgAnother.removeAll();
		imgAnother.add( label, BorderLayout.CENTER );
		imgAnother.revalidate();
		imgAnother.repaint();
		
	}
	
	public void saveUrl(final String filename, final String urlString)
	        throws MalformedURLException, IOException {
	    BufferedInputStream in = null;
	    FileOutputStream fout = null;
	    try {
	        in = new BufferedInputStream(new URL(urlString).openStream());
	        fout = new FileOutputStream(filename);

	        final byte data[] = new byte[1024];
	        int count;
	        while ((count = in.read(data, 0, 1024)) != -1) {
	            fout.write(data, 0, count);
	        }
	    } finally {
	        if (in != null) {
	            in.close();
	        }
	        if (fout != null) {
	            fout.close();
	        }
	    }
	}
		
}
