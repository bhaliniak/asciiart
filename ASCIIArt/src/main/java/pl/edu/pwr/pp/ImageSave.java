package pl.edu.pwr.pp;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;

public class ImageSave {
	
	public void savePgmToTxt(String fileName,String img){
		
		try {
			File file = new File(fileName);
			file.getParentFile().mkdirs();
			
			PrintWriter printWriter = new PrintWriter(file, "UTF-8");
			img=img.replace("<html>", "");
			img=img.replace("</html>", "");
			String imgToSave[] = img.split("<br>");
			for(int i=0;i<imgToSave.length;i++)
				printWriter.println(imgToSave[i]);
			
			printWriter.close();
			
		} catch (FileNotFoundException | UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
